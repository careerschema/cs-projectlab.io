---
title: About the CSP
subtitle: Dragging résumés into the 21st century
lastmod: 2021-05-06
socialshare: true
---

_Because changing your career shouldn't be a pain in the rear._

## Mission
The mission of the Career Schema Project is to **develop open standards and technology to reduce the time, cost, and stress of interacting in the job market.** That means better access and equity for jobseekers. It means lower costs for business. It means a coming wave of data systems that unlock new possibilities for creating, sharing, and otherwise managing career data.

## Goals

  * Drive the creation and adoption of platform-agnostic **data exchange standards** for job boards and applicant tracking systems.
  * For job seekers: to reduce time and stress by making it **easier to apply** for relevant positions.
  * For hiring managers: to reduce cost and hassle by promoting a **standardized application process**.

## Team, History, & Governance
The CSP was founded in early 2021 by [Chris Tonkinson](https://chris.tonkinson.com). Frustrated by the state of job sites and applicant tracking systems, and motivated by seeing too many people online complain about "uploading my resume, then having to enter all the same information over again manually," Chris realized that a simple concept &mdash; a well-defined schema for résumé/CV data &mdash; could have profound downstream consequences for both job seekers and hiring managers alike.

Chris currently serves as the BDPT (Benevolent Dictator Pro Tempore) until such time as the project and community have need to evolve into a more sophisticated posture. The Project is actively looking for developers, community managers, writers, recruitement insiders, and any other interested party.

## Getting Involved
There are a bunch of channels for plugging into the CSP community! All are welcome to join the project. Most official activity is concentrated in a few accounts:
  * [@careerschema](https://twitter.com/careerschema) on Twitter
  * the [careerschema](https://gitlab.com/careerschema) group on GitLab for libraries, samples, issues, the source for this website, and more
  * the [r/careerschema](https://www.reddit.com/r/careerschema) Subreddit
  * the [Career Schema Project](https://www.linkedin.com/groups/9052861/) LinkedIn group
