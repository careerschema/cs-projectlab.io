---
title: Bio
subtitle: All your résumé/CV data in one machine-readable document
lastmod: 2021-05-11
---

# Current Bio Spec (v0.2.0)

**Permalink:** [bio-v0.2.0.json](bio-v0.2.0.json)

**Full Schema:**
```js
{
  "$id": "https://cs-project.org/bio/bio-v0.2.0.json",
  "$schema": "http://json-schema.org/draft-07/schema#",
  "type": "object",
  "properties": {
    "personal": {
      "description": "General information about you",
      "type": "object",
      "properties": {
        "name": {
          "description": "Your full legal name",
          "type": "string"
        },
        "nickname": {
          "description": "What you prefer to be called",
          "type": "string"
        },
        "email": {
          "description": "Your email address",
          "type": "string"
        },
        "phone": {
          "description": "Your phone number",
          "type": "string"
        },
        "summary": {
          "description": "A brief introduction, overview, or mission statement",
          "type": "string"
        },
        "locations": {
          "description": "Where you live, work, or are willing to relocate in order to work",
          "type": "array",
          "items": [
            {
              "type": "string"
            }
          ]
        },
        "links": {
          "description": "A list of sites, pages, profiles, projects, or other resources of interest",
          "type": "array",
          "items": [
            {
              "type": "object",
              "properties": {
                "name": {
                  "description": "The title of this resource",
                  "type": "string"
                },
                "uri": {
                  "description": "The identifier of this resource",
                  "type": "string"
                }
              },
              "required": [
                "uri"
              ]
            }
          ]
        },
        "skills": {
          "description": "A list of individual skills you want to call attention to",
          "type": "array",
          "items": [
            {
              "type": "object",
              "properties": {
                "name": {
                  "description": "The name of the skill",
                  "type": "string"
                },
                "proficiency": {
                  "description": "What is your level of mastery for the skill",
                  "type": "string",
                  "enum": [
                    "fundamental",
                    "intermediate",
                    "advanced"
                  ]
                }
              },
              "required": [
                "name"
              ]
            }
          ]
        }
      },
      "required": [
        "name"
      ]
    },
    "education": {
      "description": "A list of your academic degrees",
      "type": "array",
      "items": [
        {
          "type": "object",
          "properties": {
            "institution": {
              "description": "The name of the school / college / university",
              "type": "string"
            },
            "degrees": {
              "description": "A list of degrees conferred by the institution",
              "type": "array",
              "items": [
                {
                  "type": "object",
                  "properties": {
                    "name": {
                      "description": "The level of degree",
                      "type": "string",
                      "enum": [
                        "associates",
                        "bachelors",
                        "masters",
                        "doctoral"
                      ]
                    },
                    "subject": {
                      "description": "Subject matter or area of study",
                      "type": "string"
                    },
                    "span": {
                      "type": "object",
                      "properties": {
                        "start": {
                          "description": "When study for this degree began",
                          "type": "string"
                        },
                        "end": {
                          "description": "When this degree was completed / conferred",
                          "type": "string"
                        }
                      }
                    },
                    "summary": {
                      "description": "A brief introduction, overview, or mission statement",
                      "type": "string"
                    },
                    "details": {
                      "description": "A list of accomplishments, projects, achievements, or other relevant specifics",
                      "type": "array",
                      "items": [
                        {
                          "type": "string"
                        }
                      ]
                    }
                  },
                  "required": [
                    "name"
                  ]
                }
              ]
            }
          },
          "required": [
            "institution",
            "degrees"
          ]
        }
      ]
    },
    "experience": {
      "description": "A list of organizations you've worked for",
      "type": "array",
      "items": [
        {
          "type": "object",
          "properties": {
            "organization": {
              "description": "Name of the company or organization you worked for",
              "type": "string"
            },
            "industry": {
              "description": "The industry did the company operated in",
              "type": "string"
            },
            "locations": {
              "description": "Where the company was located",
              "type": "array",
              "items": [
                {
                  "type": "string"
                }
              ]
            },
            "summary": {
              "description": "Brief overview of the company",
              "type": "string"
            },
            "positions": {
              "description": "A list of titles you held at the company",
              "type": "array",
              "items": [
                {
                  "type": "object",
                  "properties": {
                    "title": {
                      "description": "The title you held at the company",
                      "type": "string"
                    },
                    "span": {
                      "description": "When you held this title at this company",
                      "type": "object",
                      "properties": {
                        "start": {
                          "description": "When you first held this title",
                          "type": "string"
                        },
                        "end": {
                          "description": "When you last held this title",
                          "type": "string"
                        }
                      },
                      "required": [
                        "start"
                      ]
                    },
                    "links": {
                      "description": "A list of sites, pages, profiles, projects, or other resources of interest",
                      "type": "array",
                      "items": [
                        {
                          "type": "object",
                          "properties": {
                            "name": {
                              "description": "The title of this resource",
                              "type": "string"
                            },
                            "uri": {
                              "description": "The identifier of this resource",
                              "type": "string"
                            }
                          },
                          "required": [
                            "uri"
                          ]
                        }
                      ]
                    },
                    "summary": {
                      "description": "A brief overview of what you did in this role",
                      "type": "string"
                    },
                    "details": {
                      "description": "A list of accomplishments, projects, achievements, or other relevant specifics",
                      "type": "array",
                      "items": [
                        {
                          "type": "string"
                        }
                      ]
                    }
                  },
                  "required": [
                    "title",
                    "span"
                  ]
                }
              ]
            }
          },
          "required": [
            "organization",
            "positions"
          ]
        }
      ]
    },
    "credentials": {
      "description": "A list of supplemental bona fides",
      "type": "array",
      "items": [
        {
          "type": "object",
          "properties": {
            "name": {
              "description": "Title of the credential",
              "type": "string"
            },
            "kind": {
              "description": "The type of credential",
              "type": "string",
              "enum": [
                "appointment",
                "award",
                "certification",
                "clearance",
                "license",
                "membership",
                "other"
              ]
            },
            "span": {
              "description": "The time span over which this credential is held",
              "type": "object",
              "properties": {
                "start": {
                  "description": "The time this credential was first valid",
                  "type": "string"
                },
                "end": {
                  "description": "The time this credential expried/s",
                  "type": "string"
                }
              }
            },
            "issuer": {
              "description": "Organization which certifies the credential",
              "type": "string"
            },
            "identifier": {
              "description": "The unique ID number assigned to this credential",
              "type": "string"
            },
            "summary": {
              "description": "A brief overview of this credential",
              "type": "string"
            },
            "details": {
              "description": "A list of accomplishments, projects, achievements, or other relevant specifics",
              "type": "array",
              "items": [
                {
                  "type": "string"
                }
              ]
            }
          }
        }
      ]
    }
  },
  "required": [
    "personal"
  ]
}
```

---
# Changelog
### v0.2.0
  * Added descriptions to all properties.
  * Added top-level `credentials` key to capture appointments, awards, certifications, clearances, licenses, memberships, and other bona fides.
  * [schema-v0.2.0.json permalink](schema-v0.2.0.json)

### v0.1.0
  * Initial draft.
  * Includes top-level `personal`, `education`, and `experience` keys.
  * [schema-v0.1.0.json permalink](schema-v0.1.1.json)
