---
title: The Career Schema Project
subtitle: A gentle introduction
summary: It's 2021, and finding a job is still a royal pain. What should you put in your résumé? How should you structure it? And for goodness' sake, _WHAT_ is the point of...
author: Chris Tonkinson
date: 2021-05-04
tags:
  - announcements
socialshare: true
---
### What's Wrong?
It's 2021, and finding a job is still a royal pain.

What should you put in your résumé? How should you structure it?

And for goodness' sake, _WHAT_ is the point of uploading a résumé when applying for a job, if the applicant tracking system ("ATS") requires you to enter all the same information over again manually?

![what was the point?](elaboration.png)

Of course we have optical character recognition ("OCR") systems that _try_ to parse your (usually Word or PDF) résumé and automatically fill out the ATS database, but if you've used one of these in the last ever, you know how well they work.

What if a computer could read your résumé correctly, completely, instantly? What if the **only** thing you had to do to apply for a job, was to simply upload a single file? What if, after you spent an hour typing your professional history into, say, LinkedIn, you could push an export button and retrieve such a computer-readable file? What if you could take that same file and upload it to, say, Indeed, and have your profile corrected/completed instantly, instead of typing it all out again? And what if we - as humanity - could use this form of black magic as a wedge to pry cover letters away from the the cold dead fingers of the industry?

What if, indeed.

### Small Ideas, _Executed_
Today the Career Schema Project humbly, but proudly, presents to the world the initial alpha draft of its flagship schema, [bio-v0.1.0.json](/bio/bio-v0.1.0.json). CSP will adhere to the [JSON Schema](https://json-schema.org/) specifications and will each individually follow [Semantic Versioning](https://semver.org/).

Yes, "bio" is a terrible name - we're working on it. But this schema describes a document that at once replaces both résumés and CVs with a single document format that is both machine readable and, if you've got the stomach for editing [JSON](https://en.wikipedia.org/wiki/JSON) by hand, human editable.

### Status:
This is still early. We're talking _Iron Man Mark I_ early. But the Project is moving quickly and we expect a release candidate spec by the end of Q2 2021.

![Iron Man Mark I Suit](iron-man-mark-i.png)

There are still a bevy of attributes to be added to the spec, a lot of bickering over which attributes are required vs optional, and a ton of documentation to be written, not to mention software libraries, test suites, conversion tools, and the list goes on for quite a while. Nevertheless, while Google hoovers up all of your data for advertising, Spotify tries to fight Netflix for your subscription money, and Windows 10 treats your desktop like a billboard, this Project **remains committed to using software for Good&trade;**.

### What's Next?
The first few phases of this Project aren't difficult tasks, technologically... so why hasn't anyone else done it yet? It boils down to a basic lack of incentive. Anyone who is looking for work knows it's a full time job, and when the pain is most acute for the technologists equipped to solve it, they've got bigger fish to fry. As for the job sites and ATS vendors, frankly there's no obvious economic motive to _increase_ portability and interoperabiilty of this data as it only serves to reudce the friction against adoption of their competition.

We don't think these are major blockers. As for developer time, we're solving that by simply taking the first step and getting the ball rolling. As for the job boards and ATS vendors (the "demand side" of the talent market) we have some good thoughts on how to update their thinking and get them involved when the time is right.

---
The short-term roadmap is fairly clear. In some semblance of order:
  1. A 1.0 schema draft, along with documentation and samples
  2. A command line interface ("CLI") tool for simple manipulation of CSP documents
  3. Software libraries for advanced manipulation of CSP documents
  4. A freely usable website for non-techies to enter their information in a simple GUI and download an CSP document
  5. Meaningful outreach to the job listing sites and ATS vendors for integration and adoption discussions

Soon we'll publish a more detailed version of this roadmap, and another time we'll post the real long-term vision stuff. But odds are, the above items will keep us busy for a while.
