---
title: Opportunity v0.1.0 Schema
subtitle: Introducing the job listing spec
summary: ...
author: Chris Tonkinson
date: 2021-05-11
tags:
  - announcements
  - spec-change
socialshare: true
---

We are introducing the first draft of the **Opportunity** schema specification today, available at [opportunity-v0.1.0.json](/opportunity/opportunity-v0.1.0.json). You can always find the most current version of the spec, along with a changelog, on the main [Opportunity](/opportunity) page.

This is the other half of the Career Schema equation, and represents a standard for the "demand side" of the job market to rally around (if job seekers are the "supply side").

### Summary
There are currently two high-level keys in the opportunity schema: `organization` and `positions`.

`organization` contains information about the hiring entity (company, startup, nonprofit, etc.) such as `name`, `locations`, `industry`, as well as a dedicated block to identify the primary point of `contact` for coordinating inquiries.

The `positions` key is a list of open positions within the organization. Each element of `positions` contains information germane to the opening, such as the `title`, `benefits`, `hours`, specific `details`, and a lot more.

### Onward!
Now the hard part. Over the coming weeks we will be working diligently to refine these standards. Both the **bio** and **opportunity** standard are independent, and each (separately) adhere to [Semantic Versioning 2.0](https://semver.org/#semantic-versioning-200) but they _do_ represent two sides of a coin. For example, you'll notice that enums contain the same allowable values (as in degree levels and kinds of credentials) and that corresponding elements SHOULD be compatible (for example, it should be straightforward to compare the skills in a **bio** to the required skills in an **opportunity**).