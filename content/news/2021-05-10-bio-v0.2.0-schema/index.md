---
title: Bio v0.2.0 Schema
subtitle: A fuller bio spec
summary: We are tagging version v0.2.0 of the bio spec today!
author: Chris Tonkinson
date: 2021-05-10
tags:
  - announcements
  - spec-change
socialshare: true
---

We are tagging version [v0.2.0](/bio/bio-v0.2.0.json) of the bio spec today!

### 100% more descriptions
Okay, okay, it doesn't actually have _100% more descriptions_. Mathematically it has infinite percent more, because v0.1.0 had **0** descriptions, and most computer environments will barf some kind of error at you for trying to divide by zero. Whatever man, math is hard.

![Math is hard](math.jpg)

My innumeracy aside, I **also** don't know what the best practice is for description tense, punctuation, tone, et cetera, but as George S. Patton is famously quoted:

> A good plan violently executed now is better than a perfect plan executed next week.

In that spirit, most objects now include an explicit description: 

```js
        "name": {
          "description": "Your full legal name",
          "type": "string"
        },
```

The big change in v0.2.0, and the reason for bumping the minor version number, is the inclusion of the new top-level `credentials` key. This is intended to cover things like appointments, awards, certifications, clearanaces, licenses, and memberships (and a catch-all "other"). The full addition looks like this:

```js
    "credentials": {
      "description": "A list of supplemental bona fides",
      "type": "array",
      "items": [
        {
          "type": "object",
          "properties": {
            "name": {
              "description": "Title of the credential",
              "type": "string"
            },
            "kind": {
              "description": "The type of credential",
              "type": "string",
              "enum": [
                "appointment",
                "award",
                "certification",
                "clearance",
                "license",
                "membership",
                "other"
              ]
            },
            "span": {
              "description": "The time span over which this credential is held",
              "type": "object",
              "properties": {
                "start": {
                  "description": "The time this credential was first valid",
                  "type": "string"
                },
                "end": {
                  "description": "The time this credential expried/s",
                  "type": "string"
                }
              }
            },
            "issuer": {
              "description": "Organization which certifies the credential",
              "type": "string"
            },
            "identifier": {
              "description": "The unique ID number assigned to this credential",
              "type": "string"
            },
            "summary": {
              "description": "A brief overview of this credential",
              "type": "string"
            },
            "details": {
              "description": "A list of accomplishments, projects, achievements, or other relevant specifics",
              "type": "array",
              "items": [
                {
                  "type": "string"
                }
              ]
            }
          }
        }
      ]
    }
```

## Examples

Suppose a US Armed Services member wanted to capture her security clearance using the `credential` section:

```js
{
  "credentials": [
    {
      "name": "CAC",
      "kind": "clearance",
      "span": {
        "start": "2021-05-01",
        "end": "2025-05-01"
      },
      "identifier": "132-95156A",
      "summary": "Secret Clearance",
      "details": [
        "GS-5",
        "SSGT"
      ]
    }
  ]
}
```

Or let's suppose a seasoned Software Architect wanted to share that they have belonged to the ACM for three decades:

```js
{
  "credentials": [
    {
      "name": "ACM (Professional Membership)",
      "kind": "association",
      "span": {
        "start": "1985-07-12"
      },
      "identifier": "318651p"
    }
  ]
}
```

And one final example, the Registered Nurse and Prescirptive Authority licenses granted by Harrisburg to an Advanced Practice Provider:

```js
{
  "credentials": [
    {
      "name": "Registered Nurse",
      "kind": "license",
      "span": {
        "start": "2010-07-21",
        "end": "2019-11-30"
      },
      "issuer": "Pennsylvania",
      "identifier": "RN834658"
    },
    {
      "name": "Prescriptive Authority",
      "kind": "license",
      "span": {
        "start": "2014-11-18",
        "end": "2019-09-30"
      },
      "issuer": "Pennsylvania",
      "identifier": "641581"
    },
  ]
}
```

Combined with `personal`, `education`, and `experience`, the new `credentials` section rounds out the rough structure of where I wanted to take the bio schema. It's not very complex, but it really doesn't need to be.

