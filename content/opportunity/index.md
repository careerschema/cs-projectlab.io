---
title: Opportunity
subtitle: An industry standard for job listings
lastmod: 2021-05-11
---

# Current Opportunity Spec (v0.1.0)

**Permalink:** [opportunity-v0.1.0.json](opportunity-v0.1.0.json)

**Full Schema:**
```js
{
  "$id": "https://cs-project.org/opportunity/opportunity-v0.1.0.json",
  "$schema": "http://json-schema.org/draft-07/schema#",
  "type": "object",
  "properties": {
    "organization": {
      "type": "object",
      "properties": {
        "contact": {
          "type": "object",
          "properties": {
            "name": {
              "description": "e.g. John Smith",
              "type": "string"
            },
            "title": {
              "description": "e.g. Senior Manager of Meetings",
              "type": "string"
            },
            "email": {
              "description": "e.g. john@example.org",
              "type": "string"
            },
            "phone": {
              "description": "e.g. (123) 555-0100",
              "type": "string"
            },
            "address": {
              "description": "e.g. 123 Street Rd, Township, AZ 12345",
              "type": "string"
            }
          }
        },
        "name": {
          "description": "e.g. Example Corp",
          "type": "string"
        },
        "industry": {
          "description": "e.g. Telecommunications",
          "type": "string"
        },
        "size": {
          "description": "e.g. 500-1000",
          "type": "string"
        },
        "public": {
          "description": "if publicly traded company, provide ticker symbol (e.g. XMPL)",
          "type": "string"
        },
        "locations": {
          "type": "array",
          "items": [
            {
              "description": "e.g. 123 Street Rd, Exampletown, MN 45678",
              "type": "string"
            }
          ]
        },
        "links": {
          "type": "array",
          "items": [
            {
              "type": "object",
              "properties": {
                "name": {
                  "description": "e.g. Example Corp Homepage",
                  "type": "string"
                },
                "uri": {
                  "description": "e.g. https://example",
                  "type": "string"
                }
              },
              "required": [
                "uri"
              ]
            }
          ]
        },
        "mission": {
          "description": "e.g. To put a telephone in every household",
          "type": "string"
        },
        "vision": {
          "description": "e.g. To end boredom",
          "type": "string"
        },
        "values": {
          "type": "array",
          "items": [
            {
              "description": "e.g. Integrity",
              "type": "string"
            }
          ]
        },
        "summary": {
          "description": "e.g. Example Corp has been around selling phones since 1978",
          "type": "string"
        },
        "details": {
          "type": "array",
          "items": [
            {
              "description": "e.g. Global leaders in Bakelite phones",
              "type": "string"
            }
          ]
        }
      },
      "required": [
        "name",
        "summary"
      ]
    },
    "positions": {
      "type": "array",
      "items": [
        {
          "type": "object",
          "properties": {
            "title": {
              "description": "e.g. Assistant (to the) Manager of Meetings",
              "type": "string"
            },
            "reference_id": {
              "description": "e.g. REQ-D654 (can be internal requisition number",
              "type": "string"
            },
            "summary": {
              "description": "e.g. Call, reschedule, facilitate, and take minutes for meetings",
              "type": "string"
            },
            "details": {
              "type": "array",
              "items": [
                {
                  "description": "e.g. Update the TPS report cover sheets every Monday",
                  "type": "string"
                }
              ]
            },
            "salary": {
              "type": "object",
              "properties": {
                "low": {
                  "description": "e.g. $45k/yr",
                  "type": "string"
                },
                "high": {
                  "description": "e.g. $k60/yr",
                  "type": "string"
                }
              },
              "required": [
                "low"
              ]
            },
            "locations": {
              "type": "array",
              "items": [
                {
                  "description": "e.g. 123 Street Rd, Exampletown, MN 45678",
                  "type": "string"
                }
              ]
            },
            "department": {
              "description": "e.g. Project Management",
              "type": "string"
            },
            "hours": {
              "description": "e.g. 9-5, M-F",
              "type": "string"
            },
            "benefits": {
              "type": "object",
              "properties": {
                "pto": {
                  "description": "e.g. 3 weeks",
                  "type": "string"
                },
                "sick": {
                  "description": "e.g. 5 days",
                  "type": "string"
                },
                "holidays": {
                  "description": "e.g. 7 (paid)",
                  "type": "string"
                },
                "retirement": {
                  "description": "e.g. 401(k)",
                  "type": "string"
                },
                "retirement_match": {
                  "description": "e.g. up to 4%",
                  "type": "string"
                },
                "health": {
                  "description": "e.g. HSA, PPO",
                  "type": "string"
                },
                "dental": {
                  "description": "e.g. yes",
                  "type": "string"
                },
                "vision": {
                  "description": "e.g. yes",
                  "type": "string"
                },
                "other": {
                  "type": "array",
                  "items": [
                    {
                      "description": "e.g. $40/mo. gym stipend",
                      "type": "string"
                    }
                  ]
                }
              }
            },
            "required": {
              "type": "object",
              "properties": {
                "skills": {
                  "type": "array",
                  "items": [
                    {
                      "type": "object",
                      "properties": {
                        "name": {
                          "description": "e.g. C++",
                          "type": "string"
                        },
                        "proficiency": {
                          "description": "e.g. advanced",
                          "type": "string",
                          "enum": [
                            "fundamental",
                            "intermediate",
                            "advanced"
                          ]
                        }
                      },
                      "required": [
                        "name"
                      ]
                    }
                  ]
                },
                "education": {
                  "type": "array",
                  "items": [
                    {
                      "type": "object",
                      "properties": {
                        "degree": {
                          "description": "e.g. bachelors",
                          "type": "string",
                          "enum": [
                            "associates",
                            "bachelors",
                            "masters",
                            "doctoral"
                          ]
                        },
                        "subject": {
                          "description": "e.g. Mathematics",
                          "type": "string"
                        }
                      }
                    }
                  ]
                },
                "experience": {
                  "type": "array",
                  "items": [
                    {
                      "type": "string"
                    }
                  ]
                },
                "ceredentials": {
                  "type": "array",
                  "items": [
                    {
                      "type": "object",
                      "properties": {
                        "kind": {
                          "description": "e.g. certification",
                          "type": "string",
                          "enum": [
                            "appointment",
                            "award",
                            "certification",
                            "clearance",
                            "license",
                            "membership",
                            "other"
                          ]
                        },
                        "name": {
                          "description": "e.g. PMP",
                          "type": "string"
                        }
                      }
                    }
                  ]
                }
              }
            },
            "preferred": {
              "type": "object",
              "properties": {
                "skills": {
                  "type": "array",
                  "items": [
                    {
                      "type": "object",
                      "properties": {
                        "name": {
                          "description": "e.g. C++",
                          "type": "string"
                        },
                        "proficiency": {
                          "description": "e.g. advanced",
                          "type": "string",
                          "enum": [
                            "fundamental",
                            "intermediate",
                            "advanced"
                          ]
                        }
                      },
                      "required": [
                        "name"
                      ]
                    }
                  ]
                },
                "education": {
                  "type": "array",
                  "items": [
                    {
                      "type": "object",
                      "properties": {
                        "degree": {
                          "description": "e.g. associates",
                          "type": "string",
                          "enum": [
                            "associates",
                            "bachelors",
                            "masters",
                            "doctoral"
                          ]
                        },
                        "subject": {
                          "description": "e.g. Public Relations",
                          "type": "string"
                        }
                      }
                    }
                  ]
                },
                "experience": {
                  "type": "array",
                  "items": [
                    {
                      "type": "string"
                    }
                  ]
                },
                "ceredentials": {
                  "type": "array",
                  "items": [
                    {
                      "type": "object",
                      "properties": {
                        "kind": {
                          "description": "e.g. clearance",
                          "type": "string",
                          "enum": [
                            "appointment",
                            "award",
                            "certification",
                            "clearance",
                            "license",
                            "membership",
                            "other"
                          ]
                        },
                        "name": {
                          "description": "e.g. Top Secret",
                          "type": "string"
                        }
                      }
                    }
                  ]
                }
              }
            }
          },
          "required": [
            "title",
            "summary",
            "required"
          ]
        }
      ]
    }
  },
  "required": [
    "organization",
    "positions"
  ]
}
```

---
# Changelog
### v0.1.0
  * Initial draft.
